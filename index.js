const http = require("http");

const port = 4000;

//Create a simple server and the followign routes with their corresponding HTTP methods and responses:

http.createServer((request, response) => {

	console.log(`Connected to localhost:${port}`)
	// If the url is http://localhost:4000/ , send a response "Welcoem to Booking System"

		if (request.url == "/") {
			response.writeHead(200, {"Content-Type": "text/plain"});
			response.end("Welcome to Booking System!");
		} else if ( request.url == "/profile") {
			response.writeHead(200, {"Content-Type": "text/plain"});
			response.end("Welcome to your profile!")
		} else if (request.url == "/courses") {
			response.writeHead(200, {"Content-Type" : "text/plain"});
			response.end("Here's our courses available");
		} else if (request.url == "/addcourse") {
			response.writeHead(200, {"Content-Type" : "text/plain"});
			response.end("Add a course to our resources");
		} else if (request.url == "/updatecourse") {
			response.writeHead(200, {"Content-Type" : "text/plain"});
			respose.end("Update a course to our resources");
		} else if (request.url == "/archivecourses") {
			response.writeHead(200, {"Content-Type" : "text/plain"});
			response.end("Archive to our resources")
		} else {
			response.writeHead(200, {"Content-Type" : "text/plain"});
			response.end("Cannot be found");
		}

}).listen(port);
